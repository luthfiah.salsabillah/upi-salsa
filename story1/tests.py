from django.test import TestCase, Client

# Create your tests here.
class Testing(TestCase):
    def test_url_kuliah(self):
        response = Client().get('/story1/story1/')
        self.assertEquals(response.status_code, 200)
    def test_tulisan2(self):
        response = Client().get('/story1/story1/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Luthfiah Salsabillah Iskandar", html_kembalian)