from django.urls import path
import upisalsa.urls
from . import views

app_name = 'matkul'

urlpatterns = [
    path('kuliah/', views.saveMatkul, name= 'kuliah'),
    path('kuliah/readMatkul/', views.readMatkul, name='readMatkul'),
    path('kuliah/readMatkul/delete/<str:mata_kuliah>', views.delete, name = 'delete'),
    

]