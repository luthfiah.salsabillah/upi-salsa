
from django import forms
from .models import mataKuliah

class inputForm(forms.ModelForm):
	attrs = {'type' : 'text'}
	
	mata_kuliah = forms.CharField(label='Mata Kuliah', required=False, max_length = 30, widget = forms.TextInput(attrs=attrs))
	dosen_pengajar = forms.CharField(label='Dosen Pengajar', required=False, max_length = 30, widget = forms.TextInput(attrs=attrs))
	sks = forms.CharField(label='Sks', required=False, max_length = 6, widget = forms.TextInput(attrs=attrs))
	deskripsi = forms.CharField(label='Deskripsi', required=False, max_length = 60, widget = forms.TextInput(attrs=attrs))

	class Meta:
		model = mataKuliah
		fields = ['mata_kuliah','dosen_pengajar','sks','deskripsi']
	

	error_messages = {
		'required' : 'Please Type'
	}
	
	

