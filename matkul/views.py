from django.shortcuts import render, get_object_or_404, redirect
# from django.http import HttpResponseRedirect
from .forms import inputForm
from .models import mataKuliah, matkulku
# Create your views here.
def readMatkul(request):
    kuliah = mataKuliah.objects.all()
    response = {'Kuliah': kuliah,}
    html = 'kuliah.html'
    return render(request, html, response)

def saveMatkul(request):
    form = inputForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            save = form.save(commit=False)
            save.save()
            # matkul_id = request.POST.get('matkul_id', False)
            # matkul = mataKuliah.objects.get(matkul_id=matkul_id)
            
            # try:
            #     myMatkul = matkulku.objects.get(user=request.user)
            #     myMatkul.mataKuliahs.add(matkul)
            #     myMatkul.save()
            # except Exception as e:
            #     myMatkul = matkulku.objects.create(
            #         user = request.user,
            #     )
            #     myMatkul.mataKuliahs.add(matkul)
            #     myMatkul.save()
        return redirect('matkul:readMatkul')
    response = {'form' : form}
    html = 'kuliah.html'
    # if request.method == "POST":
    #     matkul_id = request.POST["matkul_id"]
    #     matkul = Mobil.objects.get(id=matkul_id)
    #     try:
    #         myMatkul = matkulku.objects.get(user=request.user)
    #         myMatkul.mataKuliahs.add(matkul)
    #         myMatkul.save()
    #     except Exception as e:
    #         myMatkul = matkulku.objects.create(
    #             user = request.user,
    #         )
    #         myMatkul.mataKuliahs.add(matkul)
    #         myMatkul.save()

    # response{'mymatkul' : myMatkul}
    #     html = 'kuliah.html'
    return render(request, html , response)


def delete(request, mata_kuliah):
    mataKuliah.objects.filter(mata_kuliah=mata_kuliah).delete()
    return redirect('matkul:readMatkul')