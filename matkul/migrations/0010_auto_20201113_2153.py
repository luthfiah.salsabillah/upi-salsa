# Generated by Django 3.1.2 on 2020-11-13 14:53

from django.db import migrations


class Migration(migrations.Migration):
    atomic = False
    dependencies = [
        ('matkul', '0009_auto_20201113_2150'),
    ]

    operations = [
        migrations.RenameField(
            model_name='matakuliah',
            old_name='id',
            new_name='matkul_id',
        ),
    ]
