from django.test import TestCase, Client

# Create your tests here.
class Testing123(TestCase):
    def test_url_kuliah(self):
        response = Client().get('/kuliah/kuliah/')
        self.assertEquals(200, response.status_code)
    def test_url_readMatkul(self):
        response = Client().get('/kuliah/kuliah/readMatkul/')
        self.assertEquals(200, response.status_code)
    def test_tulisan2(self):
        response = Client().get('/kuliah/kuliah/')
        html = response.content.decode('utf8')
        self.assertIn("Halo! Silakan Tambahkan Mata Kuliah", html)