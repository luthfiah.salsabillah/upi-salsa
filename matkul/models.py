from django.db import models
from django.contrib.auth.models import User

# from upisalsa import settings
# Create your models here.
class mataKuliah(models.Model):
    matkul_id = models.IntegerField(primary_key=True or None)
    mata_kuliah = models.CharField(max_length=30)
    dosen_pengajar = models.CharField(max_length=30)
    sks = models.CharField(max_length=6)
    deskripsi = models.TextField(max_length=60)
    def __str__(self):
        return "{}.{}".format(self.id, self.mata_kuliah)

class matkulku(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mataKuliahs = models.ManyToManyField(mataKuliah)
    def __str__(self):
        return self.user.username
