from django.test import TestCase, Client

# Create your tests here.
class Testing123(TestCase):
    def test_url_main(self):
        response = Client().get('/')
        self.assertEquals(200, response.status_code)
    def test_html_template_di_url_main(self) :
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base.html')
    def test_tulisan_main(self) :
        response = Client().get('/')
        html = response.content.decode('utf8')
        self.assertIn("Hello! I'm Luthfiah Salsabillah Iskandar", html)
        self.assertIn("AboutMe", html)
        self.assertIn("Setiap orang memiliki waktunya masing-masing", html)
        self.assertIn("Thank you for loving me", html)
        self.assertIn("ContactMe", html)
    def test_url_profile(self):
        response = Client().get('/profile/')
        self.assertEquals(200, response.status_code)
    def test_tulisan_profil(self) :
        response = Client().get('/profile/')
        html = response.content.decode('utf8')
        self.assertIn("I'm Luthfiah Salsabillah", html)
        self.assertIn("I am an Information System student at University of Indonesia", html)
        self.assertIn("Public Speaking", html)
        self.assertIn("Leadership", html)
        self.assertIn("Time Management", html)
        self.assertIn("Education", html)
        self.assertIn("Experience", html)
        self.assertIn("Aktivitas Terkini", html)
        self.assertIn("Prestasi", html)
        self.assertIn("Organisasi dan Kepanitiaan", html)
        self.assertIn("Hobi", html)
        self.assertIn("Why Should You Hire Me", html)
        self.assertIn("I May Not Have Much Experience,", html)
        self.assertIn("ContactMe", html)
        self.assertIn("But I Want Try My Best", html)
    def test_html_template_di_url_profil(self) :
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'main/profile.html')
        self.assertTemplateUsed(response, 'base.html')
   


# class FunctionalTestCase(LiveServerTestCase):
#     """Base class for functional test cases with selenium."""

#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         # Change to another webdriver if desired (and update CI accordingly).
#         options = webdriver.chrome.options.Options()
#         # These options are needed for CI with Chromium.
#         options.headless = True  # Disable GUI.
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         cls.selenium = webdriver.Chrome(options=options)

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()


# class MainTestCase(TestCase):
#     def test_root_url_status_200(self):
#         response = self.client.get('/')
#         self.assertEqual(response.status_code, 200)
#         # You can also use path names instead of explicit paths.
#         response = self.client.get(reverse('main:home'))
#         response = self.client.get(reverse('main:profile'))
#         self.assertEqual(response.status_code, 200)


# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())
