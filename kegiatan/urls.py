from django.urls import path
import upisalsa.urls
from . import views

app_name = 'kegiatan'

urlpatterns = [
    path('kegiatan/', views.saveKegiatan, name= 'kegiatan'),
    path('kegiatan/readKegiatan/', views.readKegiatan, name= 'readKegiatan'),
    path('kegiatan/readKegiatan/tambah/', views.tambah, name= 'tambah'),
]