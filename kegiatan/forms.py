from django import forms
from .models import Kegiatan, Orang
class InputForm(forms.ModelForm):
	attrs = {'type' : 'text'}
	
	aktivitas = forms.CharField(label='aktivitas', required=False, max_length = 60, widget = forms.TextInput(attrs=attrs))
	
	class Meta:
		model = Kegiatan
		fields = ['aktivitas']
    
	error_messages = {
		'required' : 'Please Type'
	}

class Input2Form(forms.ModelForm):
	attrs = {'type' : 'text'}
	
	username = forms.CharField(label='username', required=False, max_length = 60, widget = forms.TextInput(attrs=attrs))
	aktivitas = forms.ModelMultipleChoiceField(queryset=Kegiatan.objects.all())
	class Meta:
		model = Orang
		fields = ['username','aktivitas']
    
	error_messages = {
		'required' : 'Please Type'
	}
