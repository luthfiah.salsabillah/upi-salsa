from django.db import models

# Create your models here.
class Kegiatan(models.Model) :
    aktivitas = models.CharField(max_length = 60)

    def __str__(self):
        return "{}.{}".format(self.id, self.aktivitas)

class Orang(models.Model) :
    username = models.CharField(max_length = 30)
    aktivitas = models.ForeignKey(to = Kegiatan, on_delete = models.CASCADE)

    def __str__(self):
        return "{}.{}".format(self.aktivitas, self.username)
