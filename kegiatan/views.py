from django.shortcuts import render, get_object_or_404, redirect
from .forms import InputForm, Input2Form
from .models import Kegiatan, Orang

def readKegiatan(request):
    kegiatan = Kegiatan.objects.all()
    orang = Orang.objects.all();
    response = {'kegiatan': kegiatan,
    'orang' : orang}
    html = 'kegiatan.html'
    return render(request, html, response)


def saveKegiatan(request):
    form = InputForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            save = form.save(commit=False)
            save.save()
        return redirect('kegiatan:readKegiatan')
    response = {'form' : form}
    html = 'kegiatan.html'
    return render(request, html , response)

def tambah(request):
    form2 = Input2Form(request.POST or None)
    if request.method == 'POST':
        if form2.is_valid():
            save = form2.save(commit=False)
            save.save()
        return redirect('kegiatan:readKegiatan')
    response = {'form' : form2}
    html = 'tambah.html'
    return render(request, html , response)
