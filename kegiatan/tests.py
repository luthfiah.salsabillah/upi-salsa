from django.test import TestCase, Client

from .models import Kegiatan, Orang
# Create your tests here.
class Testing(TestCase):
    def test_url_kegiatan(self):
        response = Client().get('/kegiatan/kegiatan/readKegiatan/')
        self.assertEquals(response.status_code, 200)
    def test_url_saveKegiatan(self):
        response = Client().get('/kegiatan/kegiatan/')
        self.assertEquals(response.status_code, 200)
    def test_html_template_base_di_url_kegiatan(self) :
        response = Client().get('/kegiatan/kegiatan/readKegiatan/')
        self.assertTemplateUsed(response, 'kegiatan.html')
        self.assertTemplateUsed(response, 'base.html')
    def test_model_kegiatan(self) :
        kegiatan_baru = Kegiatan.objects.create(aktivitas = 'Demo TK PPW')
        hitung_semua_kegiatan = Kegiatan.objects.all().count()
        self.assertEquals(hitung_semua_kegiatan, 1)
    def test_model_orang(self):
        kegiatan_baru = Kegiatan.objects.create(aktivitas = 'Demo TK PPW')
        orang_baru = Orang.objects.create(username = 'kak pewe', aktivitas = kegiatan_baru)
        hitung_semua_orang = Orang.objects.all().count()
        self.assertEquals(hitung_semua_orang, 1)
    def test_url_tambah(self):
        response = Client().get('/kegiatan/kegiatan/readKegiatan/tambah/')
        self.assertEquals(response.status_code, 200)




