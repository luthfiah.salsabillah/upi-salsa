from django.contrib import admin

# Register your models here.
from .models import Kegiatan, Orang
admin.site.register(Kegiatan)
admin.site.register(Orang)
