from django.forms import ModelForm
from django.contrib.auth.models import User
from django import forms
# from .models import Profile
from django.contrib.auth.forms import UserCreationForm

class CreateUserForm(UserCreationForm):
    username = forms.CharField(
        label='Username',
    )
    first_name = forms.CharField(
        label='First Name',
    )
    last_name = forms.CharField(
        label='Last Name',
    )
    email = forms.EmailField(
        label='Email',
        max_length=100,
    )
    password1 = forms.CharField(
        label='Password',
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
    )
    password2 = forms.CharField(
        label='Konfirmasi Password',
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
    )
    class Meta:
        model = User
        fields = ['username','first_name','last_name', 'email','password1', 'password2']
