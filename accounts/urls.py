from django.urls import path

from . import views

app_name = "accounts"

urlpatterns = [
    path('login', views.loginPage, name='loginPage'),
    path('login/accounts/login/register', views.registerPage, name='registerPage'),
    path('logout', views.loginPage, name='logoutPage'),
]