from django.test import TestCase, Client
from .forms import CreateUserForm
from django.urls import reverse
from .views import loginPage, registerPage, logoutPage

# Create your tests here.
class Testing(TestCase):
    def test_url_login(self):
        response = Client().get('/accounts/login/')
        self.assertEquals(response.status_code, 200)
    def test_url_register(self):
        response = Client().get('/accounts/login/accounts/login/register')
        self.assertEquals(response.status_code, 200)


class FormTest(TestCase):

    def test_form_invalid(self):
        form_login = CreateUserForm(data={})
        self.assertFalse(form_login.is_valid())

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.registerPage = reverse("accounts:registerPage")
        self.loginPage = reverse("accounts:loginPage")
        self.logoutPage = reverse("accounts:logoutPage")  