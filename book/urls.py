from django.urls import path
import upisalsa.urls
from . import views

app_name = 'book'

urlpatterns = [
    path('book/', views.bukuData, name= 'book'),
]