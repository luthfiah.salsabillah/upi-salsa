from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import bukuData, data

# Create your tests here.
class buku(TestCase):

    def setUp(self):
        self.bukuData = reverse("buku:buku")
        self.data = reverse("buku:data")
    def test_url_halamanBuku(self):
        response = Client().get('/buku/')
        self.assertEquals(response.status_code, 200)

    def test_url_buku_use_right_function(self):
        found = resolve(self.bukuData)
        self.assertEqual(found.func, bukuData)
    def test_url_data_use_right_function(self):
        found = resolve(self.data)
        self.assertEqual(found.func, data)

