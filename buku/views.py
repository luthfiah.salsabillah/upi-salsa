from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import requests
import json

# Create your views here.
def bukuData(request):
    response = {}
    return render(request, 'buku.html', response)

def data(request):
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + request.GET['q']
    link = requests.get(url)
    data = json.loads(link.content)
    return JsonResponse(data, safe=False)

