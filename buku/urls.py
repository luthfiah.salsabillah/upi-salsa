from django.urls import path
import upisalsa.urls
from . import views

app_name = 'buku'

urlpatterns = [
    path('buku/', views.bukuData, name= 'buku'),
    path('data/', views.data, name='data'),
]